<?php

namespace App\Http\Controllers\Test;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class TestController extends Controller
{

    public function index() {
        return view('test.test');
    }

    public function show () {
        $user = new User();
        $users = $user->latest()->paginate(5);
        return response()->json([
            'data' => $users
        ]);
    }
}
