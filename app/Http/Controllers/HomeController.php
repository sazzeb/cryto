<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified'], ['except' => 'bitcoin']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function bitcoin(){
        $client = new \GuzzleHttp\Client([
            'header' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']
        ]);
        $request = $client->get('https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,LTC&tsyms=USD,EUR,GBP,NGR');
        $response = $request->getBody()->getContents();
        return $response;
    }
}
