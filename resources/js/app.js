
require('./bootstrap');

window.Vue = require('vue');


Vue.component('question', require('./components/questions/Questions').default)
Vue.component('test', require('./components/test/PaginationTest').default)



const app = new Vue({
    el: '#app'
});
